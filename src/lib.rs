use std::f64;
use std::cell::RefCell;
use std::rc::Rc;
use ndarray::prelude::*;
use wasm_bindgen::prelude::*;
use wasm_bindgen::{JsCast, Clamped};
use web_sys::console;

macro_rules! log {
    ( $( $t:tt )* ) => {
        web_sys::console::log_1(&format!( $( $t )* ).into());
    }
}


// When the `wee_alloc` feature is enabled, this uses `wee_alloc` as the global
// allocator.
//
// If you don't want to use `wee_alloc`, you can safely delete this.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;


fn window() -> web_sys::Window {
    web_sys::window().expect("no global `window` exists")
}

fn request_animation_frame(f: &Closure<dyn FnMut()>) {
    window()
        .request_animation_frame(f.as_ref().unchecked_ref())
        .expect("should register `requestAnimationFrame` OK");
}

fn document() -> web_sys::Document {
    window()
        .document()
        .expect("should have a document on window")
}

fn lerp_color(x0: f64, x1: f64, c0: [u8; 3], c1: [u8; 3], x: f64) -> [u8; 3] {
    let t = (x - x0) / (x1 - x0);
    [
        (c0[0] as f64 * (1.0 - t) + c1[0] as f64 * t) as u8,
        (c0[1] as f64 * (1.0 - t) + c1[1] as f64 * t) as u8,
        (c0[2] as f64 * (1.0 - t) + c1[2] as f64 * t) as u8,
    ]
}

const CELL_SIZE: u32 = 4;
const COL_ZERO: [u8; 3] = [255, 255, 255];
const COL_POSITIVE: [u8; 3] = [255, 0, 0];
const COL_NEGATIVE: [u8; 3] = [0, 0, 255];

struct Maxwell {
    w: usize,
    h: usize,

    /*eps: f64,
    mu: f64,
    dx: f64,
    dt: f64,*/

    cex: f64,
    chx: f64,

    ex: Array2<f64>,
    ey: Array2<f64>,
    pub hz: Array2<f64>,
}

impl Maxwell {
    fn new(w: usize, h: usize, dt: Option<f64>) -> Maxwell {
        let eps: f64 = 1.0;
        let mu: f64 = 1.0;
        let max_dt = (eps * mu / 2.0).sqrt();
        let dt = match dt {
            Some(dt) => {
                if dt > max_dt {
                    print!("Step too high, model unstable");
                }
                dt
            },
            None => max_dt
        };
        let cex = eps * dt;
        let chx = mu * dt;

        Maxwell {
            w,
            h,
            cex,
            chx,
            ex: Array2::<f64>::zeros((w, h + 1)),
            ey: Array2::<f64>::zeros((w + 1, h)),
            hz: Array2::<f64>::zeros((w, h)),
        }
    }

    fn update(&mut self) {
        for i in 0..self.w {
            for j in 1..self.h {
                unsafe {
                    *self.ex.uget_mut([i, j]) += self.cex * (*self.hz.uget([i, j]) - *self.hz.uget([i, j - 1]));
                }
            }
        }

        for i in 1..self.w {
            for j in 0..self.h {
                unsafe {
                    *self.ey.uget_mut([i, j]) -= self.cex * (*self.hz.uget([i, j]) - *self.hz.uget([i - 1, j]));
                }
            }
        }
        
        for i in 0..self.w {
            for j in 0..self.h {
                unsafe {
                    *self.hz.uget_mut([i, j]) +=
                        self.chx * (*self.ex.uget([i, j + 1]) - *self.ex.uget([i, j])) - self.chx * (*self.ey.uget([i + 1, j]) - *self.ey.uget([i, j]));
                }
            }
        }
    }

    fn get_hz_field_as_image(&self) -> Vec<u8> {
        let mut data = vec![];
        let (rows, cols) = self.hz.dim();
        for row in 0..rows {
            for col in 0..cols {
                let h = unsafe { *self.hz.uget([row, col]) };
                //let h = hz[[row, col]];
                let color = if h < 0.0 {
                    lerp_color(0.0, -1.0, COL_ZERO, COL_NEGATIVE, h)
                } else {
                    lerp_color(0.0, 1.0, COL_ZERO, COL_POSITIVE, h)
                };

                data.extend_from_slice(&color);
                data.push(255);
            }
        }
        data
    }
}


// This is like the `main` function, except for JavaScript.
#[wasm_bindgen(start)]
pub fn main_js() -> Result<(), JsValue> {
    // This provides better error messages in debug mode.
    // It's disabled in release mode so it doesn't bloat up the file size.
    #[cfg(debug_assertions)]
    console_error_panic_hook::set_once();

    let canvas = document().get_element_by_id("canvas").unwrap();
    let canvas: web_sys::HtmlCanvasElement = canvas
        .dyn_into::<web_sys::HtmlCanvasElement>()
        .map_err(|_| ())
        .unwrap();

    let ctx = canvas
        .get_context("2d")
        .unwrap()
        .unwrap()
        .dyn_into::<web_sys::CanvasRenderingContext2d>()
        .unwrap();

    let (w, h) = (200, 200);

    canvas.set_width(w as u32 * CELL_SIZE);
    canvas.set_height(h as u32 * CELL_SIZE);

    let mut model = Maxwell::new(w, h, Some(0.5));
    
    // Put a charge at the center
    model.hz[[w/2, h/2]] = 10.0;

    let f = Rc::new(RefCell::new(None));
    let g = f.clone();
    *g.borrow_mut() = Some(Closure::wrap(Box::new(move || {
        model.update();

        let data = web_sys::ImageData::new_with_u8_clamped_array_and_sh(
            Clamped(&mut model.get_hz_field_as_image()),
            w as u32,
            h as u32
        ).expect("data should have length `w*h*4`");
        ctx.put_image_data(&data, 0.0, 0.0)
            .expect("should be possible to put image data");

        request_animation_frame(f.borrow().as_ref().unwrap());
    }) as Box<dyn FnMut()>));

    request_animation_frame(g.borrow().as_ref().unwrap());

    Ok(())
}

