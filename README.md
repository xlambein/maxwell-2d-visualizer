# 2D Maxwell Equations Visualizer

![](images/001.gif)

## To-do

Here are the next steps for this project (more-or-less in order):

* [x] display chess board in canvas
* [x] animate chess board
* [x] compute and display z-axis H field
* [x] adapt canvas size automatically
* [ ] border conditions for FDTD
* [x] optimization to handle more pixels
* [ ] interact with H field (e.g. click = input a temporary charge)
* [ ] modify the medium µ and ε (maybe by selecting a brush)
* [ ] visualize xy-axis E field
* [ ] write a better README

## How to install

```sh
npm install
```

## How to run in debug mode

```sh
# Builds the project and opens it in a new browser tab. Auto-reloads when the project changes.
npm start
```

## How to build in release mode

```sh
# Builds the project and places it into the `dist` folder.
npm run build
```

## How to run unit tests

```sh
# Runs tests in Firefox
npm test -- --firefox

# Runs tests in Chrome
npm test -- --chrome

# Runs tests in Safari
npm test -- --safari
```

